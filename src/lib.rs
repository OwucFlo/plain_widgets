extern crate plain_ui;

use plain_ui::*;

pub trait Widget<S, T: Theme, R: Renderer<T>, I> {
    fn min_size(&self, state: &S, theme: &mut T) -> [i32; 2];
    fn render(&self, state: &S, rect: Rect, theme: &mut T, renderer: &mut R);
    fn handle_input(&self, state: &mut S, rect: Rect, theme: &mut T, input: I);
}

pub struct TextButton<S> {
    pub text: String,
    pub on_press: fn(&mut S),
    pub on_release: fn(&mut S)
}

pub struct Grid<W> {
    pub width: usize,
    pub widgets: Vec<W>
}

impl<S, T, R, I, W> Widget<S, T, R, I> for Grid<W>
    where T: Theme, R: Renderer<T>, W: Widget<S, T, R, I>
{
    fn min_size(&self, state: &S, theme: &mut T) -> [i32; 2] {
        [0; 2]
    }

    fn render(&self, state: &S, rect: Rect, theme: &mut T, renderer: &mut R) {
        
    }

    fn handle_input(&self, state: &mut S, rect: Rect, theme: &mut T, input: I) {
        
    }
}

/*pub fn button_min_size<R: Renderer>(r: &mut R, text: &str) -> [f32; 2] {
    let border = r.border_size(1);
    let text_size = r.text_min_size((1, 0), text);
    [text_size[0] + border * 2.0, text_size[1] + border * 2.0]
}

pub fn button<R: Renderer>(r: &mut R, down: bool, text: &str, rect: Rect) {
    let border = r.border_size(1);

    let min_size = button_min_size(r, text);

    let bordered = (rect.0, [min_size[0].max(rect.1[0]), min_size[1].max(rect.1[1])]);

    let borderless = (
        [rect.0[0] + border, rect.0[1] + border],
        [bordered.1[0] - border * 2.0, bordered.1[1] - border * 2.0],
    );

    let style = if down { (1, 1) } else { (1, 0) };
    r.rect(bordered, style);
    r.text(borderless, style, (1, 0), text);
}*/
